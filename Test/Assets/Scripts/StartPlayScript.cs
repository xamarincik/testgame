﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StartPlayScript : MonoBehaviour
{
    int state = 0;
    GameController gameController;
    // Start is called before the first frame update
    void Start()
    {
        gameController = transform.GetComponentInParent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (state == 0)
        {
            transform.GetComponent<TextMeshProUGUI>().enabled = true;
            if (Input.GetMouseButton(0))
            {
                state = 1;
                gameController.SetGameState(state);
                transform.GetComponent<TextMeshProUGUI>().enabled = false;
            }
        }
    }
    public void SetState(int state)
    {
        this.state = state;
    }
}
