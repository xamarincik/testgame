﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.tag.Equals("Target")&&!collision.gameObject.tag.Equals("Enemy") && !collision.gameObject.tag.Equals("Target1") && !collision.gameObject.tag.Equals("Target2")) { 
        GameObject player = GameObject.FindGameObjectWithTag("MainCamera");
        player.GetComponent<Aim>().GameState(3);
            player = GameObject.FindGameObjectWithTag("Player");
            player.GetComponent<PlayerScript>().LossOfHealth(Random.Range(30,40));
            Destroy(gameObject);
        }
    }
}
