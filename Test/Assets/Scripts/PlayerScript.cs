﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerScript : MonoBehaviour
{
    [SerializeField]
    Rigidbody bulletPrefab;
    [SerializeField]
    int hp=100, staticHp = 100;
    RaycastHit hit;
    int max_damage = 40;
    GameObject[] target;
    Image healthBar,background;
    TextMeshProUGUI healthCount;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectsWithTag("Target");

        healthBar = transform.GetChild(1).GetChild(1).GetComponent<Image>();
        background = transform.GetChild(1).GetChild(0).GetComponent<Image>();
        healthCount = transform.GetChild(1).GetChild(2).GetComponent<TextMeshProUGUI>();
        healthCount.text = staticHp + "";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PauseGame(bool paused)
    {
        if (paused)
        {
            healthBar.enabled = false;
            background.enabled = false;
            healthCount.enabled = false;
        }
        else
        {
            healthBar.enabled = true;
            background.enabled = true;
            healthCount.enabled = true;
        }
    }
    public void Restart()
    {
        target = GameObject.FindGameObjectsWithTag("Target");
        healthCount.text = staticHp + "";
        hp = staticHp;
        healthBar.fillAmount = 1; 
    }
    public void SetMaxDamage(int damage)
    {
        max_damage = damage;
    }
    public void LossOfHealth(int hp_d)
    {
        this.hp -= hp_d;
        healthCount.text = this.hp + "";
        float tmp = 1 - (1 - (float)hp_d / (float)staticHp);
        healthBar.fillAmount -= tmp;
        if (hp <= 0)
        {
            GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().SetGameState(3);
        }
        //проверка и вызов ui в случае конца хп
    }
    public void Shoting(Vector3 forward)
    {
        Vector3 point;

            point = hit.point;
            Rigidbody inst = Instantiate(bulletPrefab, transform.GetChild(0).GetChild(0).transform.position, Quaternion.identity);
            GameObject bullet = GameObject.FindGameObjectWithTag("Bullet");
            bullet.GetComponent<PlayerBulletScript>().setPoint(point, forward);
            bullet.GetComponent<PlayerBulletScript>().setDamage(CalculateDamage(hit));
        


    }
   public int CalculateDamage(RaycastHit hit)
    {
        this.hit = hit;
        switch (hit.collider.gameObject.tag)
        {
            case "Target":
                {
                    return max_damage-10;
                }
                break;
            case "Target1":
                {
                    return max_damage-5;
                }
                break;
            case "Target2":
                {
                    return max_damage;
                }
                break;
            case "Enemy":
                {
                    int[] dmg = new int[] { max_damage-15, max_damage - 16, max_damage - 17, max_damage - 18, max_damage - 19, max_damage - 20, max_damage - 21,
                        max_damage -22, max_damage-23, max_damage-24, max_damage-25 };

                    float distance = Vector3.Distance(hit.point, target[0].transform.position);
                    for (int i = 1; i < target.Length; i++)
                    {
                        float tmpDist = Vector3.Distance(hit.point, target[i].transform.position);
                        if (tmpDist < distance)
                        {
                            distance = tmpDist;
                        }

                    }
                    return dmg[(int)distance-1];
                }
                break;
            default: return 0;
        }
    }
}
