﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class EnemyScript : MonoBehaviour
{
    [SerializeField]
    Rigidbody bulletPrefab;
    [SerializeField]
    int hp,staticHp = 120;
    Image healthBar,background;
    TextMeshPro healthCount;
    //будет равна true когда снаряд игрока будет уничтожен и будет равна false после выстрела по игроку
    bool shot = false;
    // Start is called before the first frame update
    void Start()
    {
        healthBar = transform.GetChild(3).GetChild(1).GetComponent<Image>();
        //  healthBar.fillAmount =hp;
        // healthBar.fillAmount -= 0.5f;
        background = transform.GetChild(3).GetChild(0).GetComponent<Image>();
        healthCount = transform.GetChild(3).GetChild(2).GetComponent<TextMeshPro>();
        healthCount.text = staticHp + "";

    }

    // Update is called once per frame
    void Update()
    {

        if (shot&&hp>0)
        {
            if (Camera.main.transform.localPosition.y > 19)
            {
                double cordShotZ;
                System.Random rd = new System.Random();
                cordShotZ = rd.NextDouble() * (2.0 - 2.0) + (-2.0);
                Vector3 direction = new Vector3(10, 5, 0);
                Vector3 startPosition = new Vector3(transform.position.x + 1, transform.position.y, transform.position.z);
                Rigidbody inst = Instantiate(bulletPrefab, startPosition, Quaternion.identity);
                inst.velocity = direction * 3f;
                // inst.AddForce(transform.forward*30, ForceMode.Impulse);
                GameObject bullet = GameObject.FindGameObjectWithTag("EnemyBullet");
                bullet.transform.position = Vector3.Lerp(bullet.transform.position, direction, Time.deltaTime);
                shot = false;
            }
        }
    }
    void DrawHp()
    {

    }
    public void Shot(bool shot)
    {
        this.shot = shot;
    }
    public void PauseGame(bool paused)
    {
        if (paused)
        {
            healthBar.enabled = false;
            healthCount.enabled = false;
            background.enabled = false;
        }
        else
        {
            healthBar.enabled = true;
            healthCount.enabled = true;
            background.enabled = true;
        }
    }
    public void Restart()
    {
        healthCount.text = staticHp + "";
        hp = staticHp;
        healthBar.fillAmount = 1;
    }
    public void LossOfHealth(int hp_d)
    {
        this.hp -= hp_d;
        healthCount.text = this.hp + "";
        float tmp =1-  (1-(float)hp_d / (float)staticHp);
        Debug.Log(tmp + "persent"+"static = "+staticHp+"this hp = "+this.hp);
        healthBar.fillAmount -= tmp;
        if (hp <= 0)
        {
            GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().SetGameState(2);
        }
        //проверка и вызов ui в случае конца хп
    }
}
