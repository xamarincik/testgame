﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerBulletScript : MonoBehaviour
{
    int damage = 0;
    Vector3 point;
    Vector3 forw;
    Rigidbody rb;
    float Timer = 0;
    Camera camera;
    // Start is called before the first frame update
    void Start()
    {
        // Canvas.F
        //GameObject scope = GameObject.FindGameObjectWithTag("Scobe");
        //scope.active = false;
        //  camera = GetComponentInChildren<Camera>();
        forw.y += 0.5f;
        rb = GetComponent<Rigidbody>();
        camera = transform.GetChild(0).GetComponent<Camera>();
       // camera.enabled = false;
        rb.AddForce(forw * 30, ForceMode.Impulse);
        //   transform.GetChild(0).transform.localRotation = new Quaternion(0, 90, 0, 1);
        // rb.drag = 0;
    }

    // Update is called once per frame
    void Update()
    {

        transform.LookAt(point);

        Timer += Time.deltaTime;
       // if (Timer > 0.1f) camera.enabled = true;
        if (Timer > 0.5f)
        {
            rb.velocity = Vector3.Lerp(rb.velocity, transform.forward * 100, Time.deltaTime * 2);
        }

       // Debug.Log(rb.velocity);
        //rb.AddForce(transform.forward * 3, ForceMode.Impulse);


        //if (transform.position.z > 176)
        //    Destroy(gameObject);

        //rb.AddForce(transform.forward * 1f, ForceMode.Impulse);
        // transform.LookAt(point);
        //   camera.transform.LookAt(transform.position);
        //  camera.transform.position= new Vector3(-camera.transform.position.x+2.4f, camera.transform.position.y+0.8f, camera.transform.position.z);
        //  camera = new Camera();
        //    camera.transform.rotation = Quaternion.Euler(0, -90, 0);
        //    camera.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y-1, -4);
        //  camera.transform.position = transform.position + point;
        // camera.transform.localPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        //  camera.transform.rotation = Quaternion.Euler(0, -90, 0);
        // rb.AddForce(transform.forward * 1f, ForceMode.Impulse);
        //  transform.LookAt(point);
        //  transform.position = Vector3.Lerp(transform.position, point, Time.deltaTime*2) ;
        // rb.velocity = point;

    }
    private void OnCollisionEnter(Collision collision)
    {
        GameObject enemy = GameObject.FindGameObjectWithTag("Enemy");
        if (collision.gameObject.tag.Equals("Enemy")|| collision.gameObject.tag.Equals("Target")|| collision.gameObject.tag.Equals("Target1")|| collision.gameObject.tag.Equals("Target2"))
        {
            enemy.GetComponent<EnemyScript>().LossOfHealth(damage);
            damage = 0;
        }
        enemy.GetComponent<EnemyScript>().Shot(true);
        Destroy(gameObject);
    }
    public void setPoint(Vector3 point, Vector3 forw)
    {
        this.point = point;
        this.forw = forw;
    }
    public void setDamage(int dmg)
    {
        damage = dmg;
    }
}
