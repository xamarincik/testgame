﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    GameObject mainCamera, player, enemy;
    GameObject[] enemyArr;
    int state = 0;//2==win,3==loose
    int curr_ship = 1;
    // Start is called before the first frame update
    void Start()
    {
        enemyArr = GameObject.FindGameObjectsWithTag("Enemy");
        enemyArr[0].active = false;
        Debug.Log(enemyArr.Length + "lenghts");
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        player = GameObject.FindGameObjectWithTag("Player");
        enemyArr[curr_ship] = GameObject.FindGameObjectWithTag("Enemy");
    }

    // Update is called once per frame
    void Update()
    {
        if (state == 0)
        {
            player.GetComponent<PlayerScript>().PauseGame(true);
            enemyArr[curr_ship].GetComponent<EnemyScript>().PauseGame(true);
            state = -1;
        }
      else  if (state == 1)
        {
            state = -1;
            mainCamera.GetComponent<Aim>().GameState(state);
            player.GetComponent<PlayerScript>().PauseGame(false);
            enemyArr[curr_ship].GetComponent<EnemyScript>().PauseGame(false);
        }
        else if (state == 2)
        {
            player.GetComponent<PlayerScript>().PauseGame(true);
            enemyArr[curr_ship].GetComponent<EnemyScript>().PauseGame(true);
            state = -1;
            if (curr_ship == 1)
            {
                enemyArr[1].active = false;
                enemyArr[0].active = true;
                curr_ship = 0;
            }
            else
            {
                enemyArr[0].active = false;
                enemyArr[1].active = true;
                curr_ship = 1;
            }
            player.GetComponent<PlayerScript>().Restart();
            enemyArr[curr_ship].GetComponent<EnemyScript>().Restart();
            mainCamera.GetComponent<Aim>().GameState(4);
            transform.GetChild(0).GetComponent<StartPlayScript>().SetState(0);
            state = -1;
        }
        else if (state == 3)
        {
            player.GetComponent<PlayerScript>().PauseGame(true);
            enemyArr[curr_ship].GetComponent<EnemyScript>().PauseGame(true);
            player.GetComponent<PlayerScript>().Restart();
            enemyArr[curr_ship].GetComponent<EnemyScript>().Restart();
            mainCamera.GetComponent<Aim>().GameState(4);
            transform.GetChild(0).GetComponent<StartPlayScript>().SetState(0);
            state = -1;
        }

    }
    public void SetGameState(int game_state)
    {
        state = game_state;
    }
}
